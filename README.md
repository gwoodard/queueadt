# Queue Abstract Data Type Assignment #

### **About:** ###

This repo represents the source for a Queue ADT Assignment where a user will input integers until the option for Yes or No is typed and then print the input and point to the position in the list 

### **IDE:**
CodeRunner

### **Input:** ###
Random Integers

### **Language:** ###
C++