/* 

George Woodard
11/13/2014
Programming Assignment
Header Queue ADT

*/

#include <string>
using namespace std;

typedef string itemType;
struct nodeType;


//Method/pointer Decleration
class QueueADT
{
public:
	QueueADT();
	~QueueADT();
	bool isEmpty() const;
	void enqueue(itemType newItem);
	itemType getfront() const;
	void dequeue();



private:
	nodeType *front;
	nodeType *rear;
};