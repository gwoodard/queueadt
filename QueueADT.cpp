/* 

George Woodard
11/13/2014
Programming Assignment
Implementation Queue ADT

*/

#include <iostream>
#include "QueueADT.h"

using namespace std;

typedef nodeType *nodePtr;

struct nodeType{
	itemType item;
	nodePtr next;
};

//Constructor declares the front and the rear pointer as NULL
QueueADT::QueueADT()
{
	front = rear = NULL;
}

//Destructor for the Queue, checks if not NULL and says tempPtr is related to front while front points to next then deletes the memory space for tempPtr
QueueADT::~QueueADT(){
	nodePtr tempPtr;
	while(front != NULL)
	{
		tempPtr = front ;
		front = tempPtr -> next;
		delete tempPtr;
	}
}

//Boolean expression to check if the list is empty and will return empty list.
bool QueueADT::isEmpty() const
{
	return (front == NULL);
}

//
void QueueADT::enqueue(itemType newItem)
{
	nodePtr newPtr = new nodeType;
	newPtr->item = newItem;
	newPtr->next = NULL;
	if(front == NULL)
		front = rear = newPtr;
	else
	{
		rear->next = newPtr;
		rear = newPtr;
	}
}

//Will check to see if the pointer front is empty and will return the item or return NULL if empty
itemType QueueADT::getfront() const{
	if(front != NULL)
		return front->item;

	else
		return NULL;
}


//Makes the relation of each pointer pointing
void QueueADT::dequeue()
{
	nodePtr tempPtr = front;
	front = tempPtr->next;
	delete tempPtr;
	if(front == NULL)
		rear = NULL;

}